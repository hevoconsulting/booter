package com.hevo.booter.repository;

import java.util.List;

import com.hevo.booter.dao.Books;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReadingListRepository extends JpaRepository<Books, Long> {
    List<Books> findByReader(String reader);
}
